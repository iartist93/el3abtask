﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
	
	//-----------------------------------------------------------------------||   
    // container is filled with small circles
    public GameObject container; 
	public float circleReduis = 100f;
	public Text sortBtnText;
	public Dropdown shapeOptions;

	//-----------------------------------------------------------------------||    
	private float angel;							// the angel between the small circles in the circle shape
	private float x, y;								// store the new calculated coordinates of the small circles
	private float rand_reduis;						// the generated random reduis for the circle shape in the rotation animation
	private bool  rotate = false;					// toggle between true/false each time the user press rotate button
	private bool  sorted = false;					// set to true after the sort button pressed
	private int   next = 0;							// store the next index in the rotation cycle
	private int   numberOfCircles;					// used alot
	private Vector2 new_pos;						// the new calculated position for each small circle after sorting

	//-----------------------------------------------------------------------||   
	void Start()
	{
		numberOfCircles = container.transform.childCount;
	}

	//-----------------------------------------------------------------------||    
	// onClick on ("sort button") this function will be called and excuted
    public void CenterBtn()
    {
		if (!sorted) {								// if the circles not sorted
			SortCircles ();
			sorted = true;							// can't sort again	
			sortBtnText.text = "Rotate";			// and then change the title and function of the button to rotate
		} 
		else { 										// else if already sorted
			ToggleRotation ();						// toggle rotation each time we click on the button
		}
		shapeOptions.gameObject.SetActive (false);

    }

	//-----------------------------------------------------------------------||
	private void SortCircles() 
	{
		// loop to scan each child inside the container
		for (int i = 0; i < numberOfCircles; i++)
		{
			// set child position depending on its index and its shape
			if (shapeOptions.value == 0)
				new_pos = CalculatePosition (i);
			else
				new_pos = CalculateDiamondLocationFor (i);

			// animated soting
			StartCoroutine (MoveTo (i, new_pos));

			// give each circle a random color for fun :D
			container.transform.GetChild (i).GetComponent<Image>().color = new Color(Random.Range(0, 1.0f), Random.Range(0, 1.0f), Random.Range(0, 1.0f), 1.0f); 
		}	

	}
	//-----------------------------------------------------------------------||
    // function responsible to calculate position for circle shape 
	private Vector2 CalculatePosition(int index)
	{
		// Calculate the angel bwtween each small circle
		angel = 360.0f / numberOfCircles;

		// Calcuate the x,y postion for each small circle besed on this angel
		x = circleReduis * Mathf.Cos (angel * index * Mathf.Deg2Rad);
		y = circleReduis * Mathf.Sin (angel * index * Mathf.Deg2Rad);

		return new Vector3(x, y, 0);
    }
    

	//-----------------------------------------------------------------------||
	// function responsible to calculate position for diamond shape
	private Vector2 CalculateDiamondLocationFor(int index) {

		// Divide the circles number into 4 groups/sides
		float div = Mathf.Ceil(numberOfCircles / 4.0f);
	
		if (index < div) {										// Top Right Side
			x = index  * 4;
			y = numberOfCircles - x;
			return new Vector2 (x, y)  * 4;
		} else if (index < 2 * div) {							// Bottom Right Side
			x = numberOfCircles - ((index - div) * 4);
			y = numberOfCircles - x;
			return new Vector2 (x, -y)  * 4;
		} else if (index < 3 * div) {							// Bottom Left Side
			x = (index - 2 * div) * 4;
			y = numberOfCircles - x;
			return new Vector2 (-x, -y) * 4;					
		} else {												// Top Left Side
			x = numberOfCircles - ((index - 3 * div) * 4);
			y = numberOfCircles - x;
			return new Vector2 (-x, y)  * 4;					
		}
	}

	//-----------------------------------------------------------------------||
	// Lerp circles to its new position
	IEnumerator MoveTo(int index, Vector3 newPos) 
	{
		while (container.transform.GetChild (index).transform.localPosition != newPos) 
		{
			container.transform.GetChild (index).transform.localPosition = Vector3.Lerp (container.transform.GetChild (index).transform.localPosition, newPos, 0.1f);	
			yield return null;
		}
	}

	//-----------------------------------------------------------------------||
	// toggle rotation ON/OFF when the button is pressed after sorting
	public void ToggleRotation()
	{
		if (!rotate) {
			rotate = true;
			sortBtnText.text = "Stop";
		} 
		else {
			rotate = false;
			sortBtnText.text = "Rotate";
		}
	}

	//-----------------------------------------------------------------------||
	// if rotate is true, change the position of the small circles each frame to rotate CCW
	void Update() 
	{
		if (rotate) {

			// rotate by one degree each frame
			next++;

			// next is ranged form [0, number of circles]
			if (next > numberOfCircles) {
				next = 0;
			}

			// calculate the next position for each circles based on its shape
			// 0.Circle
			if (shapeOptions.value == 0) {				

				// generate a radnom positive reduis  while rotation for fun
				rand_reduis = Mathf.Abs (Mathf.Sin (Time.time) * 25);

				for (int i = 0; i < numberOfCircles; i++) {
					x = (circleReduis + rand_reduis) * Mathf.Cos (angel * (i + next) * Mathf.Deg2Rad);
					y = (circleReduis + rand_reduis) * Mathf.Sin (angel * (i + next) * Mathf.Deg2Rad);
					container.transform.GetChild (i).transform.localPosition = new Vector2 (x, y);
				}	
			} 

			// 1.Diamond
			else if (shapeOptions.value == 1) {
				for (int index = 0; index < numberOfCircles; index++) {
					// Move each small circle to its neighbour position
					container.transform.GetChild (index).transform.localPosition = CalculateDiamondLocationFor ((index + next) % numberOfCircles);
				}	
			}
		}
	}
}



